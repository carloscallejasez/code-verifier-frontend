import axios from 'axios'

export default axios.create(
    {
        baseURL: 'http://localhost:8000/api', //La url base será completada con los endpoints de nuestra api
        responseType: 'json', 
        timeout: 6000

    }

)